# A Research on Piezoelectric Elements as a new sustainable energy resource with the emphasis on Roads, Railways and Pathways

---
<!-- Uncomment following line if Extended Essay Submitted -->
<!-- Extended Essay Handed in and Project Archived. If interested in the Project feel free to copy the repository. No commits accepted! -->

**No commits accepted!** Feel free to copy the repository for Personal Projects!
---

## 1. General

This repository is used by my Supervisor [@ivo-bloechliger](https://github.com/ivo-bloechliger) and me to work on my IB Extended Essay. It consist of coding a program to meassure and evaluate the electric outcome of a Piezoelectric Element, which could potentially be used as an alternative Energy Resource.

---

## 2. Resources and Coding Language

### 2.1 Resources

1. Pietzoelectric Element <!-- Add Link []()-->
<!-- Add Further Components if needed -->

### 2.2 Language

<!-- Add Languages -->
Added Soon!

## Copyright

 <p xmlns:cc="http://creativecommons.org/ns#" >This work is licensed under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p> 